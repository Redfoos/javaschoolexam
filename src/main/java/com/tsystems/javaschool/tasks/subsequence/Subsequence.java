package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;


public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int pointer = 0;
        int index = 0;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) {
            return true;
        }


        if (x.get(0) instanceof Integer)  {

            for (Object i : x) {
                for (int j = pointer; j < y.size(); j++) {
                    if ((int) i == (int)y.get(j)) {
                        index++;
                        pointer = j;
                        break;
                    }
                }
            }
        } else {
            for (Object i : x) {
                String tmp = (String) i;
                for (int j = pointer; j < y.size(); j++) {
                    if (tmp.equals(y.get(j))) {
                        index++;
                        pointer = j;
                        break;
                    }
                }
            }
        }

        return index == x.size();
    }
}
