package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int minimalCapasity = 3;
        int counter = 0;
        int addendum = 3;
        long startNum = 3;

        for (Integer num : inputNumbers) {
            if (num == null) throw new CannotBuildPyramidException();
        }

        int expNum = inputNumbers.size();
        //count size of pyramid array
        while (startNum != expNum) {
            if (startNum > expNum) {
                throw new CannotBuildPyramidException();
            }
            startNum += addendum;
            addendum++;
            counter++;
        }
        inputNumbers.sort(Comparator.comparingInt(integer -> integer));
        minimalCapasity+= counter * 2;
        int[][] result = new int[counter + 2][minimalCapasity];

        //fill the pyramid
        int currIndex = result[0].length / 2;
        int prevIndex = currIndex;
        int i = 0;
        int numOfIter = 1;
        int pointer = 0;
        while (prevIndex != 0) {
            prevIndex = currIndex;
            for (int l = 0; l < numOfIter; l++) {
                Integer num = inputNumbers.get(pointer);
                result[i][currIndex] = num;
                pointer++;
                currIndex += 2;
            }
            i++;
            currIndex = prevIndex - 1;
            numOfIter++;
        }

        return result;
    }

}
