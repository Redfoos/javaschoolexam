package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private List<String> parseFormula(String statement) {
        List<String> formulaList = new ArrayList<>();
        int j = 0;
        int counterOfOperator = 0;
        int counterOfDots = 0;
        String numberAcc = "";
        String[] splittedStatement = statement.split("");
        while (j < splittedStatement.length) {
            String curr = splittedStatement[j];
            if (curr.equals(",")) {
                return null;
            }
            if (counterOfOperator > 1 && (curr.equals("+") || curr.equals("-") || curr.equals("*") || curr.equals("/"))) {
                return null;
            }
            if ((curr.equals("*")) || (curr.equals("/")) || (curr.equals("+")) || (curr.equals("-")) || (curr.equals("(")) || curr.equals(")")) {
                if (!numberAcc.equals("")) {
                    if (counterOfDots > 1) return null;
                    counterOfDots = 0;
                    formulaList.add(numberAcc);
                    numberAcc = "";
                }
                if (curr.equals("(") || curr.equals(")")) {
                    counterOfOperator = 0;
                    formulaList.add(curr);
                    j++;
                } else {
                    formulaList.add(curr);
                    j++;
                    counterOfOperator++;
                }
                counterOfOperator++;
            } else {
                if (curr.equals(".")) {
                    counterOfDots++;
                }
                numberAcc += curr;
                j++;
                counterOfOperator = 0;
            }
        }
        if (!numberAcc.equals("")) {
            formulaList.add(numberAcc);
        }
        return formulaList;

    }

    private List<String> reversePolishNotation(List<String> formulaList) {
        Stack<String> operationStack = new Stack<>();
        List<String> outputList = new ArrayList<>();
        int counterOfOpenBracer = 0;
        int counterOfCloseBracer = 0;
        for (String it : formulaList){
            try {
                Double.parseDouble(it);
                outputList.add(it);
            } catch (NumberFormatException ex) {
                if (it.equals("(")) {
                    counterOfOpenBracer++;
                    operationStack.push(it);
                }
                if (it.equals("+") || it.equals("-")) {
                    if(operationStack.isEmpty() || operationStack.peek().equals("(")) {
                        operationStack.push(it);
                        continue;
                    }

                    if(operationStack.peek().equals("+") || operationStack.peek().equals("-")) {
                        outputList.add(operationStack.pop());
                        operationStack.push(it);

                    }

                    if (operationStack.peek().equals("*") || operationStack.peek().equals("/")) {
                        while (true) {
                            if (operationStack.isEmpty()) break;
                            if ((operationStack.peek().equals("+") || operationStack.peek().equals("-") || operationStack.peek().equals("("))) break;
                            outputList.add(operationStack.pop());
                        }
                        operationStack.push(it);
                    }
                }

                if (it.equals("*") || it.equals("/")) {
                    if (operationStack.isEmpty() || operationStack.peek().equals("(")) {
                        operationStack.push(it);
                        continue;
                    }

                    if (operationStack.peek().equals("*") || operationStack.peek().equals("/")) {
                        outputList.add(operationStack.pop());
                        operationStack.push(it);
                    }

                    if (operationStack.peek().equals("+") || operationStack.peek().equals("-")) {
                        operationStack.push(it);
                    }
                }

                if (it.equals(")")) {
                    counterOfCloseBracer++;
                    while (!operationStack.isEmpty()) {
                        String itm = operationStack.pop();
                        if (!itm.equals("(")) {
                            outputList.add(itm);
                        }
                    }
                }
            }

        }
        if (!operationStack.isEmpty()) {
            while (!operationStack.isEmpty()) {
                outputList.add(operationStack.pop());
            }
        }
        if (counterOfCloseBracer != counterOfOpenBracer) return null;

        return outputList;

    }

    public String evaluate(String statement) {
        if (statement == null || statement == "") return null;

        List<String> formulaList;
        List<String> outputList;
        List<Double> resList = new ArrayList<>();

        formulaList = parseFormula(statement);
        if (formulaList == null) return null;

        outputList = reversePolishNotation(formulaList);
        if (outputList == null) return null;


        //Calculate
        for (String str : outputList) {
            if (str.equals("+")) {

                Double tmpRes = (resList.remove(resList.size() - 1) + resList.remove(resList.size() - 1));
                resList.add(resList.size(), tmpRes);
                continue;
            }

            if (str.equals("-")) {
                Double tmpRes = (resList.remove(resList.size() - 2) - resList.remove(resList.size() - 1));
                resList.add(resList.size(), tmpRes);
                continue;
            }

            if (str.equals("*")) {
                Double tmpRes = (resList.remove(resList.size() - 1) * resList.remove(resList.size() - 1));
                resList.add(resList.size(), tmpRes);
                continue;
            }

            if (str.equals("/")) {
                Double divident = resList.remove(resList.size() - 2);
                Double divider = resList.remove(resList.size() - 1);
                if (divider == 0) {
                    return null;
                }
                Double tmpRes = (divident / divider);
                resList.add(resList.size(), tmpRes);
                continue;
            }
            resList.add(Double.parseDouble(str));

        }

        //Format answer
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(resList.get(0));

    }
}